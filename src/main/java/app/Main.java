package app;

import java.math.BigInteger;
import java.util.Arrays;

public class Main {



    public static void main(String[] args) {

        Main app = new Main();

        String amount = "999";
        if (!app.approval(amount)) {
            System.out.println(amount + " does not require approval");
        }
        amount = "2000";
        if (app.approval(amount)) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean approval(String amount) {
        ApprovalRequestAmount requestAmount = new ApprovalRequestAmount(amount);
        return requestAmount.requiresApproval();
    }

}
