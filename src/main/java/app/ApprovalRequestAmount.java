package app;

import java.math.BigInteger;

public class ApprovalRequestAmount {

    private final BigInteger amount;
    private final BigInteger approvalThreshold = BigInteger.valueOf(1000);
    private final BigInteger maximumAllowableRequestAmount = BigInteger.valueOf(Integer.MAX_VALUE);
    private final BigInteger minimumAllowableRequestAmount = BigInteger.valueOf(Integer.MIN_VALUE);

    public ApprovalRequestAmount(String incoming) {
        BigInteger requestAmount = new BigInteger(incoming);

        if (requestedAmountTooLarge(requestAmount))
            throw new IllegalArgumentException("Outside of allowed range");

        if (requestedAmountTooSmall(requestAmount))
            throw new IllegalArgumentException("Outside of allowed range");
        this.amount = requestAmount;
    }

    public BigInteger getAmount() {
        return amount;
    }

    public boolean requiresApproval() {
        return amount.compareTo(approvalThreshold) >= 0;
    }

    private boolean requestedAmountTooSmall(BigInteger requestAmount) {
        return requestAmount.compareTo(minimumAllowableRequestAmount) <= 0;
    }

    private boolean requestedAmountTooLarge(BigInteger requestAmount) {
        return requestAmount.compareTo(maximumAllowableRequestAmount) >= 0;
    }


}
